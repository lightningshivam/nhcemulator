package in.juspay.shivamrajesh.hostbasedcardemulation;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.ComponentCallbacks;
import android.content.ComponentName;
import android.content.Intent;
import android.nfc.cardemulation.HostApduService;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;

import in.juspay.shivamrajesh.hostbasedcardemulation.common.Headers;
import in.juspay.shivamrajesh.hostbasedcardemulation.common.Utils;

/**
 * This is a sample APDU Service which demonstrates how to interface with the card emulation support
 * added in Android 4.4, KitKat.
 *
 * <p>This sample replies to any requests sent with the string "Hello World". In real-world
 * situations, you would need to modify this code to implement your desired communication
 * protocol.
 *
 * <p>This sample will be invoked for any terminals selecting AIDs of 0xF11111111, 0xF22222222, or
 * 0xF33333333. See src/main/res/xml/aid_list.xml for more details.
 *
 * <p class="note">Note: This is a low-level interface. Unlike the NdefMessage many developers
 * are familiar with for implementing Android Beam in apps, card emulation only provides a
 * byte-array based communication channel. It is left to developers to implement higher level
 * protocol support as needed.
 */
public class CardService extends HostApduService {

    public static final String BROADCAST_ACTION = "in.juspay.shivamrajesh.hostbasedcardemulation";
    public static final String STATUS = "id_status";
    public static final String READER_MESSAGE = "reade_data";


    private static final String TAG = "CardService";

    String[] results;
    String command = "";
    int sendCounter = 0;
    String result;




    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "Service started");
        return START_STICKY;
    }


    @Override
    public void registerComponentCallbacks(ComponentCallbacks callback) {
        super.registerComponentCallbacks(callback);
    }

    /**
     * Called if the connection to the NFC card is lost, in order to let the application know the
     * cause for the disconnection (either a lost link, or another AID being selected by the
     * reader).
     *
     * @param reason Either DEACTIVATION_LINK_LOSS or DEACTIVATION_DESELECTED
     */
    @Override
    public void onDeactivated(int reason) {
        command = "";
        Log.e(TAG,"OnDeactivated: "+reason);
    }

    /**
     * This method will be called when a command APDU has been received from a remote device. A
     * response APDU can be provided directly by returning a byte-array in this method. In general
     * response APDUs must be sent as quickly as possible, given the fact that the user is likely
     * holding his device over an NFC reader when this method is called.
     *
     * <p class="note">If there are multiple services that have registered for the same AIDs in
     * their meta-data entry, you will only get called if the user has explicitly selected your
     * service, either as a default or just for the next tap.
     *
     * <p class="note">This method is running on the main thread of your application. If you
     * cannot return a response APDU immediately, return null and use the {@link
     * #sendResponseApdu(byte[])} method later.
     *
     * @param commandApdu The APDU that received from the remote device
     * @param extras A bundle containing extra data. May be null.
     * @return a byte-array containing the response APDU, or null if no response APDU can be sent
     * at this point.
     */
    // BEGIN_INCLUDE(processCommandApdu)
    @Override
    public byte[] processCommandApdu(byte[] commandApdu, Bundle extras) {
//        Log.i(TAG, "Received APDU: " + ByteArrayToHexString(commandApdu));
//        // If the APDU matches the SELECT AID command for this service,
//        // send the loyalty card account number, followed by a SELECT_OK status trailer (0x9000).
//        if (Arrays.equals(SELECT_APDU, commandApdu)) {
//            String account = AccountStorage.GetAccount(this);
//            byte[] accountBytes = account.getBytes();
//            Log.i(TAG, "Sending account number: " + account);
//            return ConcatArrays(accountBytes, SELECT_OK_SW);
//        } else {
//            return UNKNOWN_CMD_SW;
//        }
        if(!isForeground("in.juspay.shivamrajesh.hostbasedcardemulation")) {
            Intent dialogIntent = new Intent(this, MainActivity.class);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            dialogIntent.putExtra("Data", "Reader Detected");
            startActivity(dialogIntent);
        }
        String s = Utils.ByteArrayToHexString(commandApdu);
        Log.d(TAG, "processCommandApdu : " + s);
        String commandClass = s.substring(0, Math.min(s.length(), 8));
        Log.d(TAG, "commandClass = " + commandClass);

        if (commandClass.equalsIgnoreCase(Headers.HEADER_SELECT)) {
            // This is select command

            Intent i = new Intent(BROADCAST_ACTION);

            i.putExtra(STATUS,"Reader Detected...Sending data");

            sendBroadcast(i);
            return Utils.ConcatArrays(onCardSelect(s), Headers.RESPONSE_SELECT_OK);
        }
        else if (commandClass.equalsIgnoreCase(Headers.HEADER_SENDCOMMAND)) {

            try {
                Log.d(TAG, "catenated command  = " + new String(Utils.HexStringToByteArray(command)));
            } catch (Exception e) {
                Log.d(TAG,"Exception Generated "+command);

                e.printStackTrace();
            }


            String check = new String(Utils.HexStringToByteArray(s));
            Log.w(TAG,s+" "+check+ " "+check.contains("END_OF_COMMAND"));

            if (check.contains("END_OF_COMMAND")) {
                Log.d(TAG, "received END_OF_COMMAND");
                results = Utils.StringSplit255(onReceiveCommand(command));
                command = "";
                return Utils.ConcatArrays("RESPONSE_SENDCOMMAND_PROCESSED".getBytes(), Headers.RESPONSE_SENDCOMMAND_PROCESSED);
            }
            command += s.substring(12);

            return Utils.ConcatArrays("RESPONSE_SENDCOMMAND_OK".getBytes(), Headers.RESPONSE_SENDCOMMAND_OK);
        }
        else if (commandClass.equalsIgnoreCase(Headers.HEADER_GETDATA)) {

            result = sendResultPart(results);
            if (result.equalsIgnoreCase("END_OF_DATA")) {
                return Utils.ConcatArrays(result.getBytes(), Headers.RESPONSE_GETDATA_FINAL);
            }
            return Utils.ConcatArrays(result.getBytes(), Headers.RESPONSE_GETDATA_INTERMEDIATE);

        }
        return Utils.ConcatArrays(onReceiveCommand(s).getBytes(), Headers.RESPONSE_SELECT_OK);

    }

    public String sendResultPart (String[] results) {
        if(results ==null)
            return "END_OF_DATA";
        if (sendCounter < results.length) {
            sendCounter += 1;
            return results[sendCounter-1];
        } else {
            sendCounter = 0;
            return "END_OF_DATA";
        }

    }


    /**
     * Method that performs the transaction
     * <p/>
     * When in contact with a device having the reader app, it will send a command
     * that will call this function.
     * This is reached only after a successful first iteration of
     * onCardSelect is done
     * <p/>
     * You need to parse the received command
     * and send back a suitable message/data as per the needs of your applicaiton
     *
     * @param command - Command received from the Card Reader device
     * @return - The message that needs to be sent back to the reader.
     */
    private String onReceiveCommand(String command) {
        Log.i(TAG, "onReceiveCommand called with command = " + command);
        String actualCommand = new String(Utils.HexStringToByteArray(command));
        Log.i(TAG, "actual command = " + actualCommand);
        Intent intent = new Intent(BROADCAST_ACTION);
        intent.putExtra(READER_MESSAGE,actualCommand);
        sendBroadcast(intent);

        if (actualCommand.contains("somecommand")) {
            return "someresult";
        }
        if (actualCommand.contains("hello")) {
            return "hi there nice to met you";
        }
        if (actualCommand.contains("boom")) {
            if(MainActivity.isAuthenticated()){
                return "Card Details :q";
            }else {
                return "waiting for authentication";
            }
        }

        return "DATA_BASED_ON_COMMAND";
    }


    /**
     * Method that runs on card selection
     * <p/>
     * When first contact with a device having the reader app,
     * the select command is sent.
     * This is best used to exchange id's of of reader and card
     * <p/>
     * You need to parse the received command
     * and send back a suitable message/data as per the needs of your applicaiton
     *
     * @return - The message that needs to be sent back to the reader.
     */
    private byte[] onCardSelect(String command) {
        Log.i(TAG, "onCardSelect called with command = " + command);
//        return "PATIENT_ID_HERE".getBytes();

        String account = AccountStorage.GetAccount(this);
        byte[] accountBytes = account.getBytes();
        Log.i(TAG, "Sending account number: " + account);
        return  accountBytes;
    }

    public boolean isForeground(String myPackage) {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> runningTaskInfo = manager.getRunningTasks(1);
        ComponentName componentInfo = runningTaskInfo.get(0).topActivity;
        return componentInfo.getPackageName().equals(myPackage);
    }



}

