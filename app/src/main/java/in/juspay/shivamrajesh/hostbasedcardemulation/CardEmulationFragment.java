package in.juspay.shivamrajesh.hostbasedcardemulation;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import in.juspay.shivamrajesh.hostbasedcardemulation.adapter.IsoDepAdapter;

/**
 * Generic UI for sample discovery.
 */
public class CardEmulationFragment extends Fragment {


    public static final String TAG = "CardEmulationFragment";

    private BroadcastReceiver broadcastReceiver;

    private IsoDepAdapter isoDepAdapter;




    /** Called when sample is created. Displays generic UI with welcome text. */
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        Intent i = new Intent(getContext(), CardService.class);
        getActivity().startService(i);

        registerReciever();

    }

    private void registerReciever() {
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if(intent.hasExtra(CardService.STATUS)){
                    String status = intent.getStringExtra(CardService.STATUS);
                    ((TextView)getActivity().findViewById(R.id.some_text)).setText(status);
                }
                if(intent.hasExtra(CardService.READER_MESSAGE)){
                    String readerMessage = intent.getStringExtra(CardService.READER_MESSAGE);
//                    ((TextView)getActivity().findViewById(R.id.reader_mess)).setText(readerMessage);
                    isoDepAdapter.addMessage(readerMessage);

                }
            }
        };
        getActivity().registerReceiver(broadcastReceiver,new IntentFilter(CardService.BROADCAST_ACTION));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(broadcastReceiver!=null){
            getActivity().unregisterReceiver(broadcastReceiver);
//            MainActivity.setIsAuth(false);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_card_emulation, container, false);
        EditText account = (EditText) v.findViewById(R.id.card_account_field);
        account.setText(AccountStorage.GetAccount(getActivity()));
        account.addTextChangedListener(new AccountUpdater());

        ListView listView = (ListView)v.findViewById(R.id.listViewCard);
        isoDepAdapter = new IsoDepAdapter(getLayoutInflater());
        listView.setAdapter(isoDepAdapter);

        return v;
    }


    private class AccountUpdater implements TextWatcher {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            // Not implemented.
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            // Not implemented.
        }

        @Override
        public void afterTextChanged(Editable s) {
            String account = s.toString();
            AccountStorage.SetAccount(getActivity(), account);
        }
    }
}
