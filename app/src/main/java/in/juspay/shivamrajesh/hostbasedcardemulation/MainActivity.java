package in.juspay.shivamrajesh.hostbasedcardemulation;

import android.app.ActivityManager;
import android.app.KeyguardManager;
import android.content.ComponentName;
import android.content.Intent;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "MainActivity";

    private static final int LOCK_REQUEST_CODE = 221;
    private static final int SECURITY_SETTING_REQUEST_CODE = 233;
    private static boolean isAuth=false;

    // Whether the Log Fragment is currently shown
    private boolean mLogShown;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent intent = getIntent();
        if (intent.hasExtra("Data")){
            Log.e("=>",intent.getStringExtra("Data"));

            if(isAuth){
                startCardEmulationFragment();
            }
            else {
                authenticateApp();

            }

        }

        ((Button) findViewById(R.id.btn_reader)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                CardReaderFragment fragment = new CardReaderFragment();
                transaction.replace(R.id.sample_content_fragment, fragment);
                transaction.commit();
            }
        });
        ((Button)findViewById(R.id.btn_card)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isAuth){
                    startCardEmulationFragment();
                }else {
                    authenticateApp();
                }

            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case LOCK_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    //If screen lock authentication is success update text
                    isAuth = true;
                    startCardEmulationFragment();
                    Toast.makeText(getApplicationContext(),"Authenticated",Toast.LENGTH_SHORT).show();


                } else {
                    Toast.makeText(getApplicationContext(),"Failed to Authenticate",Toast.LENGTH_SHORT).show();
                    //If screen lock authentication is failed update text
                }
                break;
            case SECURITY_SETTING_REQUEST_CODE:
                //When user is enabled Security settings then we don't get any kind of RESULT_OK
                //So we need to check whether device has enabled screen lock or not
                if (isDeviceSecure()) {
                    //If screen lock enabled show toast and start intent to authenticate user
                    authenticateApp();
                } else {
                    //If screen lock is not enabled just update text
                }
                Toast.makeText(getApplicationContext(),"here",Toast.LENGTH_SHORT).show();


                break;
        }
    }

    //method to authenticate app
    public  void authenticateApp() {
        //Get the instance of KeyGuardManager
        KeyguardManager keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);

        //Check if the device version is greater than or equal to Lollipop(21)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //Create an intent to open device screen lock screen to authenticate
            //Pass the Screen Lock screen Title and Description
            Intent i = keyguardManager.createConfirmDeviceCredentialIntent(getResources().getString(R.string.unlock), getResources().getString(R.string.confirm_pattern));
            try {
                //Start activity for result
                startActivityForResult(i, LOCK_REQUEST_CODE);
            } catch (Exception e) {

                //If some exception occurs means Screen lock is not set up please set screen lock
                //Open Security screen directly to enable patter lock
                Intent intent = new Intent(Settings.ACTION_SECURITY_SETTINGS);
                try {

                    //Start activity for result
                    startActivityForResult(intent, SECURITY_SETTING_REQUEST_CODE);
                } catch (Exception ex) {

                    //If app is unable to find any Security settings then user has to set screen lock manually
                }
            }
        }
    }
    /**
     * method to return whether device has screen lock enabled or not
     **/
    private boolean isDeviceSecure() {
        KeyguardManager keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);

        //this method only work whose api level is greater than or equal to Jelly_Bean (16)
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN && keyguardManager.isKeyguardSecure();

        //You can also use keyguardManager.isDeviceSecure(); but it requires API Level 23

    }

    public static boolean isAuthenticated(){
        return isAuth;
    }

    public static void setIsAuth(boolean value ){
        isAuth = value;
    }
    private void startCardEmulationFragment(){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        CardEmulationFragment fragment = new CardEmulationFragment();
        transaction.replace(R.id.sample_content_fragment, fragment);
        transaction.commit();
    }


}
