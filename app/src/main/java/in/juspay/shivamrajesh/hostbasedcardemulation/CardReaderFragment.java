package in.juspay.shivamrajesh.hostbasedcardemulation;

import android.app.Activity;
import android.nfc.NfcAdapter;
import android.nfc.tech.IsoDep;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.io.IOException;
import java.util.Arrays;

import in.juspay.shivamrajesh.hostbasedcardemulation.adapter.IsoDepAdapter;
import in.juspay.shivamrajesh.hostbasedcardemulation.common.Headers;
import in.juspay.shivamrajesh.hostbasedcardemulation.common.Utils;
import in.juspay.shivamrajesh.hostbasedcardemulation.reader.TransceiveResult;


/**
 * Generic UI for sample discovery.
 */
public class CardReaderFragment extends Fragment implements LoyaltyCardReader.ReadCallBack {

    public static final String TAG = "CardReaderFragment";
    // Recommend NfcAdapter flags for reading from other Android devices. Indicates that this
    // activity is interested in NFC-A devices (including other Android devices), and that the
    // system should not check for the presence of NDEF-formatted data (e.g. Android Beam).
    public static int READER_FLAGS =
            NfcAdapter.FLAG_READER_NFC_A | NfcAdapter.FLAG_READER_SKIP_NDEF_CHECK;
    public LoyaltyCardReader mLoyaltyCardReader;
    private TextView mAccountField;

    public static int mMaxTransceiveLength;
    public static boolean mExtendedApduSupported;
    public static int mTimeout;
    IsoDepAdapter isoDepAdapter;


    /** Called when sample is created. Displays generic UI with welcome text. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_card_reader, container, false);
        if (v != null) {
            mAccountField = (TextView) v.findViewById(R.id.card_account_field);
            mAccountField.setText("Waiting...");
            ListView listView = (ListView)v.findViewById(R.id.listViewReader);
            isoDepAdapter = new IsoDepAdapter(getLayoutInflater());
            listView.setAdapter(isoDepAdapter);
            mLoyaltyCardReader = new LoyaltyCardReader(this);

            // Disable Android Beam and register our card reader callback
            enableReaderMode();
        }

        return v;
    }

    @Override
    public void onPause() {
        super.onPause();
        disableReaderMode();
    }

    @Override
    public void onResume() {
        super.onResume();
        enableReaderMode();
    }


    /**
     * Method to start the reader mode.
     * <p/>
     * If reader mode isn't enabled, devices wont interact, even when tapped together.
     * Reader mode should be enabled before tapping to be able to detect the other device.
     */
    private void enableReaderMode() {
        Log.i(TAG, "Enabling reader mode");
        Activity activity = getActivity();
        NfcAdapter nfc = NfcAdapter.getDefaultAdapter(activity);
        if (nfc != null) {
            nfc.enableReaderMode(activity, mLoyaltyCardReader, READER_FLAGS, null);
        }
    }

    /**
     * Method to stop reader mode.
     * <p/>
     * Advisable to free up the reader mode when leaving the activity.
     * If reader mode is engaged, other NFC functions like Beam and other Wallet like
     * apps wont function, so always close reader mode safely before leaving the app.
     */
    private void disableReaderMode() {
        Log.i(TAG, "Disabling reader mode");
        Activity activity = getActivity();
        NfcAdapter nfc = NfcAdapter.getDefaultAdapter(activity);
        if (nfc != null) {
            nfc.disableReaderMode(activity);
        }
    }

    @Override
    public void onAccountReceived(final String account) {
        // This callback is run on a background thread, but updates to UI elements must be performed
        // on the UI thread.
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mAccountField.setText(account);
            }
        });
    }


    /**
     * Method to start a transaction with the emulator app on the other device
     * <p/>
     * Send a custom "sendCommand" that will be read by emulator, which will
     * accordingly send back a message, that will be returned by this function.
     * <p/>
     * Usually you do not need to override this function.
     * If data is larger than 255 chars, this method will ideally
     * internally iterate over the calls to send the data.
     *
     * @param command - A string command to send to card emulator device.
     * @return - The message sent back by emulator device after receiving the command
     */
    @Override
    public String transactNfc(IsoDep isoDep, String command) throws IOException {
        Log.d(TAG, "transactNFC started");

        int resultLength = 0;
        String gotData = "", finalGotData = "";
        long timeTaken = 0;
        TransceiveResult mResult;

        Log.d(TAG, "doing transactNfc with command = " + command);

        // Split command into 255 byte parts
        String[] commandParts = Utils.StringSplit255(command);

        // Keep fetching until we reach the end
        for (int i = 0; i < commandParts.length; i++) {
            Log.d(TAG, "we are inside commandParts loop");
            Log.d(TAG, "commandParts.length = " + commandParts.length);
            Log.d(TAG, "commandParts = " + commandParts[i]);
            byte[] sendCommand = Headers.BuildSendDataApdu(i, commandParts[i]);
            Log.i(TAG, "Sending: " + Utils.ByteArrayToHexString(sendCommand));
            mResult = TransceiveResult.get(isoDep, sendCommand);
            resultLength = mResult.getLength();
            Log.i(TAG, "Received rlen : " + resultLength);
            byte[] statusWordNew = mResult.getStatusword();
            if (Arrays.equals(Headers.RESPONSE_SENDCOMMAND_OK, statusWordNew)) {
                gotData = new String(mResult.getPayload(), "UTF-8");
                Log.i(TAG, "Received: " + gotData);
            }
        }

        byte[] sendCommandFinal = Headers.BuildSendDataApdu(commandParts.length, "END_OF_COMMAND");
        Log.i(TAG, "Sending: " + Utils.ByteArrayToHexString(sendCommandFinal));
        mResult = TransceiveResult.get(isoDep, sendCommandFinal);
        resultLength = mResult.getLength();
        Log.i(TAG, "Received rlen : " + resultLength);
        byte[] statusWordNew = mResult.getStatusword();
        if (Arrays.equals(Headers.RESPONSE_SENDCOMMAND_PROCESSED, statusWordNew)) {
            gotData = new String(mResult.getPayload(), "UTF-8");
            Log.i(TAG, "Received: " + gotData);
        }

        while (true) {
            Log.d(TAG, "we are inside getData loop");
            byte[] getCommand = Headers.BuildGetDataApdu();
            Log.i(TAG, "Sending: " + Utils.ByteArrayToHexString(getCommand));
            mResult = TransceiveResult.get(isoDep, getCommand);
            resultLength = mResult.getLength();
            Log.i(TAG, "Received rlen : " + resultLength);
            statusWordNew = mResult.getStatusword();
            if (Arrays.equals(Headers.RESPONSE_GETDATA_INTERMEDIATE, statusWordNew)) {
                gotData = new String(mResult.getPayload(), "UTF-8");
                Log.i(TAG, "Received: " + gotData);
                finalGotData = finalGotData + gotData;
                Log.i(TAG, "Data transferred : " + finalGotData.length());
                Log.i(TAG, "Time taken: " + (System.currentTimeMillis() - timeTaken));
                Log.d(TAG, "Final data = " + finalGotData);
            } else if (Arrays.equals(Headers.RESPONSE_GETDATA_FINAL, statusWordNew)) {
                break;
            }
        }
        return finalGotData;
    }

    @Override
    public void onHceStarted(IsoDep isoDep) {
        Log.d(TAG, "onHCEStarted");

        //Check out support for extended APDU here if you want to
        mExtendedApduSupported = isoDep.isExtendedLengthApduSupported();
        mMaxTransceiveLength = isoDep.getMaxTransceiveLength();
        // Set and check timeout here if you want to
        mTimeout = isoDep.getTimeout();
        isoDep.setTimeout(3600);
        mTimeout = isoDep.getTimeout();

        Log.i(TAG,
                "  Extended APDU Supported = " + mExtendedApduSupported +
                        "  Max Transceive Length = " + mMaxTransceiveLength +
                        "  Timeout = " + mTimeout);

        // TODO:
        // Start sending the commands here
        // Using transactNFC(command); calls


            try {
                String message = "hello";
                while (true){
                    final String result = transactNfc(isoDep, message);
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
        //                    ((TextView)getActivity().findViewById(R.id.reader_some_text)).setText(result);
                            isoDepAdapter.addMessage(result);

                        }
                    });
                    if (result.contains(":q"))
                        break;
                    Log.d(TAG, "result = " + result);
                    message = "boom";
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
    }
}
